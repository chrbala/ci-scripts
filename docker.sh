mkdir ~/.ssh/
ssh-keyscan -t rsa bitbucket.org >> ~/.ssh/known_hosts

apt-get update

# flow dependencies
apt-get --assume-yes install ocaml libelf-dev

npm install -g yarn