### Prerequisites

* SSH_KEY environment key must be set to the bitbucket SSH key base64 encoded
* Package must have a validate script
* The docker image must be or inherit from chrbala/ci-scripts

### Usage
Consumers can run ``ci-scripts``.

### Development
Update the docker image with ``npm run docker``.