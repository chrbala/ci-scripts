#!/usr/bin/env bash

RSA_PATH=~/.ssh/id_rsa
echo $SSH_KEY | base64 --decode > $RSA_PATH
chmod 600 $RSA_PATH
eval $(ssh-agent -s)
ssh-add $RSA_PATH

yarn install
yarn run validate
