#!/usr/bin/env node

var exec = require('child_process').execFileSync;
exec(
	'bash', 
	['-e', require.resolve('./ci-scripts.sh')], 
	{ env: process.env, stdio: 'inherit' }
);
